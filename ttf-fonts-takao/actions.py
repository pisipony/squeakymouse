#!/usr/bin/python
# -*- coding: utf-8 -*-
# Licensed under the GNU General Public License, version 3.
# See the file http://www.gnu.org/copyleft/gpl.txt

from pisi.actionsapi import get
from pisi.actionsapi import pisitools

# if pisi can't find source directory, see /var/pisi/ttf-fonts-takao/work/ and:
# WorkDir="ttf-fonts-takao-"+ get.srcVERSION() +"/sub_project_dir/"

def setup():
    print('')

def build():
    print('')

def install():
    pisitools.dolib("TakaoGothic.ttf")
    pisitools.dolib("TakaoMincho.ttf")
    pisitools.dolib("TakaoPGothic.ttf")
    pisitools.dolib("TakaoPMincho.ttf")
    pisitools.dodoc("README")
    pisitools.dodoc("README.ja")
    pisitools.dodoc("ChangeLog")
    pisitools.dodoc("IPA_Font_License_Agreement_v1.0.txt")
    pisitools.domove("/usr/lib/TakaoGothic.ttf","/usr/share/fonts/TTF")
    pisitools.domove("/usr/lib/TakaoMincho.ttf","/usr/share/fonts/TTF")
    pisitools.domove("/usr/lib/TakaoPGothic.ttf","/usr/share/fonts/TTF")
    pisitools.domove("/usr/lib/TakaoPMincho.ttf","/usr/share/fonts/TTF")
    pisitools.removeDir("/usr/lib")

# Take a look at the source folder for these file as documentation.
#    pisitools.dodoc("AUTHORS", "BUGS", "ChangeLog", "COPYING", "README")

# If there is no install rule for a runnable binary, you can 
# install it to binary directory.
#    pisitools.dobin("ttf-fonts-takao")

# You can use these as variables, they will replace GUI values before build.
# Package Name : ttf-fonts-takao
# Version : 15.03
# Summary : Takao Fonts are a community developed derivatives of IPA Fonts. The main purpose of this project is to secure the possibility to maintain the fonts by the community.

# For more information, you can look at the Actions API
# from the Help menu and toolbar.
