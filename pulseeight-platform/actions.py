#!/usr/bin/python
# -*- coding: utf-8 -*-
# Licensed under the GNU General Public License, version 3.
# See the file http://www.gnu.org/copyleft/gpl.txt

from pisi.actionsapi import cmaketools
from pisi.actionsapi import get
from pisi.actionsapi import pisitools
from os import system

# if pisi can't find source directory, see /var/pisi/pulseeight-platform/work/ and:
# WorkDir="pulseeight-platform-"+ get.srcVERSION() +"/sub_project_dir/"

def setup():
    cmaketools.configure("-DCMAKE_BUILD_TYPE=release -DBUILD_SHARED_LIBS=ON", installPrefix="/usr")

def build():
    cmaketools.make("-j4")

def install():
    cmaketools.rawInstall("DESTDIR=%s" % get.installDIR())
    system("ldconfig")

# Take a look at the source folder for these file as documentation.
    pisitools.dodoc("README.md","debian/copyright", "debian/changelog.in")

# If there is no install rule for a runnable binary, you can 
# install it to binary directory.
#    pisitools.dobin("pulseeight-platform")

# You can use these as variables, they will replace GUI values before build.
# Package Name : pulseeight-platform
# Version : 2.1.0
# Summary : Support platform for libCEC and Kodi

# For more information, you can look at the Actions API
# from the Help menu and toolbar.
