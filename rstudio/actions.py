#!/usr/bin/python
# -*- coding: utf-8 -*-
# Licensed under the GNU General Public License, version 3.
# See the file http://www.gnu.org/copyleft/gpl.txt

from pisi.actionsapi import cmaketools
from pisi.actionsapi import get
from pisi.actionsapi import pisitools
from os import system

# if pisi can't find source directory, see /var/pisi/rstudio/work/ and:
a="rstudio-"+ get.srcVERSION() +"/dependencies/common/"

def setup():
    system("cd /var/pisi/rstudio-" + get.srcVERSION() + "-1/work/"+ a + " && ./install-dictionaries")
    system("cd /var/pisi/rstudio-" + get.srcVERSION() + "-1/work/"+ a + " && ./install-mathjax")
    system("cd /var/pisi/rstudio-" + get.srcVERSION() + "-1/work/"+ a + " && ./install-pandoc")
    system("cd /var/pisi/rstudio-" + get.srcVERSION() + "-1/work/"+ a + " && ./install-libclang")
    system("cd /var/pisi/rstudio-" + get.srcVERSION() + "-1/work/"+ a + " && ./install-cef")
    system("cd /var/pisi/rstudio-" + get.srcVERSION() + "-1/work/"+ a + " && ./install-packages")
    system("cd /var/pisi/rstudio-" + get.srcVERSION() + "-1/work/"+ a + " && ./install-gwt")
    cmaketools.configure("-DRSTUDIO_TARGET=Desktop -DCMAKE_BUILD_TYPE=release -DQT_QMAKE_EXECUTABLE=qmake-qt5 /"
##-DLIBR_HOME=/usr/lib/R -DLIBR_LIBRARIES=/usr/lib/R/lib /
, installPrefix="/usr")

def build():
    cmaketools.make()

def install():
    cmaketools.rawInstall("DESTDIR=%s" % get.installDIR())

# Take a look at the source folder for these file as documentation.
    system("rm " + get.installDIR() + "/usr/COPYING")
    system("rm " + get.installDIR() + "/usr/INSTALL")
    system("rm " + get.installDIR() + "/usr/NOTICE")
    system("rm " + get.installDIR() + "/usr/README.md")
    system("rm " + get.installDIR() + "/usr/SOURCE")
    system("rm " + get.installDIR() + "/usr/VERSION")
    #system("rm -rf" + get.installDIR() + "/usr/extras")
    pisitools.dodoc("CONTRIBUTING.md", "COPYING", "INSTALL", "NEWS.md", "NOTICE", "README.md", "SOURCE", "VERSION")

# If there is no install rule for a runnable binary, you can 
# install it to binary directory.
#    pisitools.dobin("rstudio")

# You can use these as variables, they will replace GUI values before build.
# Package Name : rstudio
# Version : 1.1.463
# Summary : IDE for the R language

# For more information, you can look at the Actions API
# from the Help menu and toolbar.
