#!/usr/bin/python
# -*- coding: utf-8 -*-
# Licensed under the GNU General Public License, version 3.
# See the file http://www.gnu.org/copyleft/gpl.txt

from pisi.actionsapi import autotools
from pisi.actionsapi import get
from pisi.actionsapi import pisitools
from pisi.actionsapi import libtools
from subprocess import call
from os import system

# if pisi can't find source directory, see /var/pisi/sdl-sound/work/ and:
# WorkDir="sdl-sound-"+ get.srcVERSION() +"/sub_project_dir/"

def setup():
    #For some reason pisi skips this step
    autotools.aclocal()
    libtools.libtoolize("--automake --copy --force")
    autotools.autoheader()
    autotools.automake("--foreign --add-missing --copy")
    autotools.autoconf()
    autotools.configure()

def build():
    autotools.aclocal()
    libtools.libtoolize("--automake --copy --force")
    autotools.autoheader()
    autotools.automake("--foreign --add-missing --copy")
    autotools.autoconf()
    autotools.configure()
    #autotools.automake("--foreign --add-missing --copy")
    autotools.make()

def install():
    autotools.rawInstall("DESTDIR=%s" % get.installDIR())
    system("libtool --finish /usr/lib")
    pisitools.dodoc("CHANGELOG", "COPYING", "CREDITS", "INSTALL", "README", "TODO")
# Take a look at the source folder for these file as documentation.
#    pisitools.dodoc("AUTHORS", "BUGS", "ChangeLog", "COPYING", "README")

# If there is no install rule for a runnable binary, you can 
# install it to binary directory.
#    pisitools.dobin("sdl-sound")

# You can use these as variables, they will replace GUI values before build.
# Package Name : sdl-sound
# Version : 1.0.3
# Summary : Sound libraries for SDL

# For more information, you can look at the Actions API
# from the Help menu and toolbar.
