#!/usr/bin/python
# -*- coding: utf-8 -*-
# Licensed under the GNU General Public License, version 3.
# See the file http://www.gnu.org/copyleft/gpl.txt

from pisi.actionsapi import pythonmodules
from pisi.actionsapi import pisitools

# if pisi can't find source directory, see /var/pisi/pysqlite/work/ and:
# WorkDir="pysqlite-"+ get.srcVERSION() +"/sub_project_dir/"

def build():
    pythonmodules.compile()

def install():
    pythonmodules.install()
    pisitools.domove("/usr/pysqlite2-doc/*","/usr/share/doc/pysqlite2-doc")
    pisitools.removeDir("/usr/pysqlite2-doc")

# Take a look at the source folder for these file as documentation.
#    pisitools.dodoc("AUTHORS", "BUGS", "ChangeLog", "COPYING", "README")

# If there is no install rule for a runnable binary, you can 
# install it to binary directory.
#    pisitools.dobin("pysqlite")

# You can use these as variables, they will replace GUI values before build.
# Package Name : pysqlite
# Version : 2.8.3
# Summary : Python interface to SQLite 3

# For more information, you can look at the Actions API
# from the Help menu and toolbar.
