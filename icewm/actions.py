#!/usr/bin/python
# -*- coding: utf-8 -*-
# Licensed under the GNU General Public License, version 3.
# See the file http://www.gnu.org/copyleft/gpl.txt

from pisi.actionsapi import cmaketools
from pisi.actionsapi import get
from pisi.actionsapi import pisitools

# if pisi can't find source directory, see /var/pisi/icewm/work/ and:
# WorkDir="icewm-"+ get.srcVERSION() +"/sub_project_dir/"

def setup():
    cmaketools.configure("-DCMAKE_BUILD_TYPE=release", installPrefix="/usr")

def build():
    cmaketools.make()

def install():
    cmaketools.rawInstall("DESTDIR=%s" % get.installDIR())

# Take a look at the source folder for these file as documentation.
    pisitools.dodoc("AUTHORS", "BUGS", "ChangeLog", "CHANGES", "CODE_OF_CONDUCT.md", "COMPLIANCE", "CONTRIBUTING.md", "COPYING", "INSTALL", "NEWS", "PLATFORMS", "README.md", "THANKS", "TODO", "VERSION")

# If there is no install rule for a runnable binary, you can 
# install it to binary directory.
#    pisitools.dobin("icewm")

# You can use these as variables, they will replace GUI values before build.
# Package Name : icewm
# Version : 1.4.1
# Summary : Fast and light window manager

# For more information, you can look at the Actions API
# from the Help menu and toolbar.
