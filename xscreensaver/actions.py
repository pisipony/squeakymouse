#!/usr/bin/python
# -*- coding: utf-8 -*-
# Licensed under the GNU General Public License, version 3.
# See the file http://www.gnu.org/copyleft/gpl.txt

from pisi.actionsapi import autotools
from pisi.actionsapi import get
from pisi.actionsapi import pisitools
from pisi.actionsapi import shelltools

# if pisi can't find source directory, see /var/pisi/xscreensaver/work/ and:
# WorkDir="xscreensaver-"+ get.srcVERSION() +"/sub_project_dir/"

def setup():
    autotools.configure("--mandir=%s" % get.installDIR()
    + "/usr/man --infodir=%s" % get.installDIR() + "/usr/share/info --sysconfdir=%s" % get.installDIR()
    + "/etc --datadir=%s" % get.installDIR() + "/usr/share --libexecdir=%s" % get.installDIR()
    + "/usr/libexec --with-x-app-defaults=%s" % get.installDIR() + "/usr/share/X11/app-defaults --datarootdir=%s"
    % get.installDIR() + "/usr --bindir=%s" % get.installDIR() + "/usr/bin")

def build():
    autotools.make()

def install():
    autotools.rawInstall("DESTDIR=%s" % get.installDIR())

#Working around unbudging shortcut directory issue, please disable sandboxing
    shelltools.system("mkdir " + get.installDIR() + "/usr/share/applications")
    shelltools.system("cp %s" % get.workDIR() + "/xscreensaver-5.43/driver/screensaver-properties.desktop " + get.installDIR() + "/usr/share/applications/xscreensaver-properties.desktop")
    shelltools.system("rm /usr/share/applications/xscreensaver-properties.desktop")

# Take a look at the source folder for these file as documentation.
    pisitools.dodoc("INSTALL","README", "README.hacking", "README.VMS")

# If there is no install rule for a runnable binary, you can 
# install it to binary directory.
#    pisitools.dobin("xscreensaver")

# You can use these as variables, they will replace GUI values before build.
# Package Name : xscreensaver
# Version : 5.39
# Summary : Screensaver collection for X11 systems

# For more information, you can look at the Actions API
# from the Help menu and toolbar.
