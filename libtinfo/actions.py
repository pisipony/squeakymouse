#!/usr/bin/python
# -*- coding: utf-8 -*-
# Licensed under the GNU General Public License, version 3.
# See the file http://www.gnu.org/copyleft/gpl.txt

from pisi.actionsapi import pisitools

# if pisi can't find source directory, see /var/pisi/libtinfo/work/ and:
# WorkDir="libtinfo-"+ get.srcVERSION() +"/sub_project_dir/"

def setup():
    print("")

def build():
    print("")

def install():

# Take a look at the source folder for these file as documentation.
#    pisitools.dodoc("AUTHORS", "BUGS", "ChangeLog", "COPYING", "README")

# If there is no install rule for a runnable binary, you can 
# install it to binary directory.
    pisitools.dobin("libtinfo.so")
    pisitools.domove("/usr/bin/libtinfo.so","/usr/lib")
    pisitools.removeDir("/usr/bin")

# You can use these as variables, they will replace GUI values before build.
# Package Name : libtinfo
# Version : 6.1
# Summary : Terminal info library

# For more information, you can look at the Actions API
# from the Help menu and toolbar.
