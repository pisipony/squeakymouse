#!/usr/bin/python
# -*- coding: utf-8 -*-
# Licensed under the GNU General Public License, version 3.
# See the file http://www.gnu.org/copyleft/gpl.txt

from pisi.actionsapi import autotools
from pisi.actionsapi import get
from pisi.actionsapi import pisitools
from os import system

# if pisi can't find source directory, see /var/pisi/rcpisiman/work/ and:
# WorkDir="rcpisiman-"+ get.srcVERSION() +"/sub_project_dir/"

def setup():
    print("")

def build():
    autotools.make()

def install():
    pisitools.dodir("/usr/lib")
    pisitools.dodir("/usr/lib/RCPisiman")

# domove doesn't work
    system("cp -r " + get.workDIR() + "/RCPisiman-master/repotools/ " + get.installDIR() + "/usr/lib/ && cp -r " 
 + get.workDIR() + "/RCPisiman-master/gui/ " + get.installDIR() + "/usr/lib/RCPisiman && cp -r " 
 + get.workDIR() + "/RCPisiman-master/example/ " + get.installDIR() + "/usr/lib/RCPisiman && cp -r " 
 + get.workDIR() + "/RCPisiman-master/config/ " + get.installDIR() + "/usr/lib/RCPisiman && cp -r " 
 + get.workDIR() + "/RCPisiman-master/icons/ " + get.installDIR() + "/usr/lib/RCPisiman && cp -r " 
 + get.workDIR() + "/RCPisiman-master/pisiman.py " + get.installDIR() + "/usr/lib/RCPisiman")

# Take a look at the source folder for these file as documentation.
    pisitools.dodoc("LICENSE", "README.md", "TODO")

# If there is no install rule for a runnable binary, you can 
# install it to binary directory.
#    pisitools.dobin("rcpisiman")

# You can use these as variables, they will replace GUI values before build.
# Package Name : rcpisiman
# Version : 20180424
# Summary : ISO builder for Pisi Linux

# For more information, you can look at the Actions API
# from the Help menu and toolbar.
