#!/usr/bin/python
# -*- coding: utf-8 -*-

from pisi.actionsapi import shelltools
from pisi.actionsapi import autotools
from pisi.actionsapi import pisitools
from pisi.actionsapi import get


import os

def setup():
    shelltools.export("PATH", "%s:/usr/lib/jvm/java-7-openjdk/bin" % os.environ.get("PATH"))
    shelltools.system("./bootstrap")
    pisitools.dosed("configure", "-ldts" , "-ldca")
    pisitools.dosed("xbmc/utils/SystemInfo.cpp","lsb_release -d","cat /etc/pisilinux-release")
    #shelltools.export("CXXFLAGS", "-I/usr/include/crossguid")
    autotools.rawConfigure("--disable-ccache \
                            --disable-optimizations \
                            --disable-avahi \
                            --disable-hal \
                            --enable-goom=yes \
                            --enable-openmax=no \
                            --disable-webserver \
                            --disable-libxslr \
                            --enable-lcms2=no \
                            --enable-libcap=no \
                            --enable-player=vlc,xmms\
                            --enable-gtest=no \
                            --prefix=/usr \
                            --with-ffmpeg=force")

    #pisitools.dosed("libtool", " -shared ", " -Wl,-O1,--as-needed -shared ")

def build():
    autotools.make("-lOpenGL64 -lGlu64 -lGlx64")

def install():
    autotools.rawInstall("DESTDIR=%s" % get.installDIR())
    pisitools.doman("docs/manpages/*")
    pisitools.dodoc("*.txt","LICENSE.GPL")