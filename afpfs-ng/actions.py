#!/usr/bin/python
# -*- coding: utf-8 -*-
# Licensed under the GNU General Public License, version 3.
# See the file http://www.gnu.org/copyleft/gpl.txt

from pisi.actionsapi import autotools
from pisi.actionsapi import get
from pisi.actionsapi import pisitools

# if pisi can't find source directory, see /var/pisi/afpfs-ng/work/ and:
# WorkDir="afpfs-ng-"+ get.srcVERSION() +"/sub_project_dir/"

def setup():
    autotools.configure()

def build():
    autotools.make()

def install():
    autotools.rawInstall("DESTDIR=%s" % get.installDIR())

# Take a look at the source folder for these file as documentation.
    pisitools.dodoc("AUTHORS", "Bugs.txt", "ChangeLog", "COPYING", "INSTALL", "NEWS", "README.md", "TODO")

# If there is no install rule for a runnable binary, you can 
# install it to binary directory.
#    pisitools.dobin("afpfs-ng")

# You can use these as variables, they will replace GUI values before build.
# Package Name : afpfs-ng
# Version : 20150729
# Summary : afpfs-ng/libafpclient is an open source client for the Apple Filing Protocol

# For more information, you can look at the Actions API
# from the Help menu and toolbar.
